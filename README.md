# ResumeTemplate

The project was created to provie others with a simple markup for resumes. This project utilizes LaTeX, and 
it provides the user with a guide to fill out the template resume to provide to future employers. 

Please make sure that you pull the full project or take all of the files and keep them in one directory. 
The LaTeX file will search for these images to include during the creation of the pdf file.

## Software

This project was created using TeXMaker. I highly suggest this software, but you are free to use whatever
LaTeX editor that you wish.